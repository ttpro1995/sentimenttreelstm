"""
From your shell, run

    git clone <this project repo>
    git clone https://github.com/stanfordnlp/treelstm.git
    cd treelstm
    ./fetch_and_preprocess.sh

This will download the datasets, the word vectors, and do some
preprocessing on the data.  Once this is complete, go into the
tree_rnn directory and start a Python shell.  In that shell,
we'll preprocess the word vectors.

After ./fetch_and_preprocess.sh done
run this code
"""

import data_utils
vocab = data_utils.Vocab()
vocab.load('../treelstm/data/sst/vocab-cased.txt')
words, embeddings = \
    data_utils.read_embeddings_into_numpy(
        '../treelstm/data/glove/glove.840B.300d.txt', vocab=vocab)

import numpy as np
np.save('../treelstm/data/words.npy', words)
np.save('../treelstm/data/glove.npy', embeddings)


"""
After run this code
make data folder in <this_project_root>/data
Copy
    + folder treelstm/data/sst/
    + file /treelstm/data/words.npy
    + file /treelstm/data/glove.npy
to folder we just create
"""