import pytest
from treelstm.ChildSumTreeLSTM import ChildSumTreeComposer
from treelstm.sentiment_module import SentimentModule
import numpy as np
in_dim = 300
mem_dim = 168
number_of_child = 10
num_classes = 13

class TestChildsum:
    def meow(self):
        print ("meow")

    def test_composer_output(self):
        input_x = np.random.rand(in_dim)
        h = np.random.rand(number_of_child, mem_dim)
        composer = ChildSumTreeComposer(in_dim, mem_dim, DEBUG_MODE=True)
        output = composer.forward(input_x, h)
        print (output.shape)
        assert output.shape[0]==mem_dim


    def test_composer_f(self):
        input_x = np.random.rand(in_dim)
        h = np.random.rand(number_of_child, mem_dim)
        composer = ChildSumTreeComposer(in_dim, mem_dim, DEBUG_MODE=True)

        f = composer.cal_f(input_x, h)
        print (f.shape)
        assert f.shape == (number_of_child, mem_dim)

    def test_composer_c(self):
        input_x = np.random.rand(in_dim)
        child_h = np.random.rand(number_of_child, mem_dim)
        child_c = np.random.rand(number_of_child, mem_dim)
        composer = ChildSumTreeComposer(in_dim, mem_dim, DEBUG_MODE=True)

        h = composer.cal_h(input_x, child_h, child_c)
        print ('h shape',h.shape)
        assert h.shape[0] == mem_dim

    def test_sentiment(self):

        h = np.random.rand(mem_dim)
        sentiment_module = SentimentModule(mem_dim,num_classes, True)
        yhat = sentiment_module.forward(h)
        print ('yhat shape ',yhat.shape)
        assert yhat.shape[0] == num_classes
