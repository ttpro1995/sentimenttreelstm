import numpy as np
import theano

def init_matrix(shape):
    return np.random.normal(scale=0.1, size=shape).astype(theano.config.floatX)