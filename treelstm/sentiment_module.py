import theano
import theano.tensor as T
import util
from collections import defaultdict
class SentimentModule:
    # take h => proba each class
    def __init__(self, mem_dim, num_classes, DEBUG_MODE = False):
        self.DEBUG_MODE = DEBUG_MODE
        self.mem_dim = mem_dim
        self.num_classes = num_classes
        self.W = theano.shared(util.init_matrix((mem_dim, num_classes)), name='W')
        self.b = theano.shared(util.init_matrix(num_classes))
        self.params = defaultdict()
        if (self.DEBUG_MODE):
            self.__theano_build__()

    def __theano_build__(self):
        mem_dim = self.mem_dim
        num_classes = self.num_classes
        h = T.fvector('h')

        yhat = self.get_cell(h)

        if (self.DEBUG_MODE):
            self.forward = theano.function([h], yhat, allow_input_downcast=True)

    def get_cell(self, h):
        W = self.W
        b = self.b
        # yhat = h.W + b  | (mem_dim) (mem_dim, num_class) = num_class
        yhat = T.nnet.sigmoid(T.dot(h, W) + b)
        return yhat