from treelstm.TreeLSTM import TreeLSTM
from collections import defaultdict
import numpy as np
import theano.tensor as T
import theano
import util

class ChildSumTreeComposer():
    def __init__(self, in_dim, mem_dim, DEBUG_MODE = False):
        self.DEBUG_MODE = DEBUG_MODE
        self.in_dim = in_dim
        self.mem_dim = mem_dim
        self.param = defaultdict()
        self.W_i = theano.shared(util.init_matrix([in_dim, mem_dim]))
        self.W_f = theano.shared(util.init_matrix([in_dim, mem_dim]))
        self.W_o = theano.shared(util.init_matrix([in_dim, mem_dim]))
        self.W_u = theano.shared(util.init_matrix([in_dim, mem_dim]))

        self.U_i = theano.shared(util.init_matrix([mem_dim, mem_dim]))
        self.U_f = theano.shared(util.init_matrix([mem_dim, mem_dim]))
        self.U_o = theano.shared(util.init_matrix([mem_dim, mem_dim]))
        self.U_u = theano.shared(util.init_matrix([mem_dim, mem_dim]))

        self.b_i = theano.shared(np.zeros(mem_dim))
        self.b_f = theano.shared(np.zeros(mem_dim))
        self.b_o = theano.shared(np.zeros(mem_dim))
        self.b_u = theano.shared(np.zeros(mem_dim))

        self.param = defaultdict()
        if self.DEBUG_MODE:
            self.__theano_build__()


    def __theano_build__(self):
        in_dim = self.in_dim
        mem_dim = self.mem_dim



        input_x = T.fvector('input_x') # x
        child_c = T.fmatrix('child_c') # c_k (matrix of all child, each row for a child)
        child_h = T.fmatrix('child_h') # h_k (matrix of all child)

        i, f, o, u, c, h = self.get_cell(input_x, child_h, child_c)

        # for debug and testing
        if (self.DEBUG_MODE):
            # only build the computation graph if DEBUG MODE
            self.cal_f = theano.function([input_x, child_h], f, allow_input_downcast=True)
            self.cal_h = theano.function([input_x, child_h, child_c], h, allow_input_downcast=True)

            # function
            self.forward = theano.function([input_x, child_h], o, allow_input_downcast=True)



    def get_cell(self, input_x, child_h, child_c):
        child_h_sum = T.sum(child_h, axis=0)
        W_i = self.W_i
        W_f = self.W_f
        W_o = self.W_o
        W_u = self.W_u

        U_i = self.U_i
        U_f = self.U_f
        U_o = self.U_o
        U_u = self.U_u

        b_i = self.b_i
        b_f = self.b_f
        b_o = self.b_o
        b_u = self.b_u
        # forward
        # . is T.dot()
        # * is element-wise multiplication
        # i = x.W + h_sum.U + b | (in)(in, mem) + (mem)(mem, mem) = (mem)
        i = T.nnet.sigmoid(T.dot(input_x, W_i) + T.dot(child_h_sum, U_i) + b_i)
        # f = x.W + h.U + b | (in)(in, mem) + (number_of_child, mem)(mem, mem) = (number_of_child,mem)
        f = T.nnet.sigmoid(T.dot(input_x, W_f) + T.dot(child_h, U_f) + b_f)
        # 0 =  x.W + h_sum.U + b | (in)(in, mem) + (1, mem)(mem, mem) = (mem)
        o = T.tanh(T.dot(input_x, W_o) + T.dot(child_h_sum, U_o) + b_o)
        # u = x.W + h_sum.U + b | (in)(in, mem) + (mem)(mem, mem) = (mem)
        u = T.tanh(T.dot(input_x, W_u) + T.dot(child_h_sum, U_u) + b_u)
        # C = i * u + sum(f_k) * c_k | (mem)*(mem) + (mem)*(mem) = mem
        c = i*u + T.sum(f*child_c, axis=0)
        # h = o*tanh(c) | (mem) * (mem) = mem
        h = o * T.tanh(c)
        return (i, f, o, u, c, h)



class ChildSumTreeLSTM(TreeLSTM):
    def __init__(self, config):
        super(ChildSumTreeLSTM, self).__init__(config)
        self.gate_output = config.gate_output
        if self.gate_output == None:
            self.gate_output = True

        # a function that instantiates an output module that takes the hidden state h as input
        self.output_module_fn = config.output_module_fn
        self.criterion = config.criterion

        # composition module
        self.composer = self.new_composer()
        self.composers = []

        # output module
        self.output_module = self.new_output_module()
        self.output_modules = []

    def new_composer(self):
        pass

    def new_output_module(self):
        pass
