import numpy as np

class TreeLSTM:
    def __init__(self, config):
        self.in_dim = config.in_dim
        assert self.in_dim > 0

        if config.mem_dim is not None:
            self.mem_dim = config.mem_dim
        else:
            self.mem_dim = 150

        self.mem_zeros = np.zeros(self.mem_dim)
        self.train = False

    def forward(self, tree, inputs):
        pass

    def backward(self, tree, inputs, grad):
        pass

    def training(self):
        self.train = False

    def allocate_module(self, tree, module):
        pass


    def free_module(self, tree, module):
        pass

