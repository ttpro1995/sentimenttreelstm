from __future__ import print_function
import theano
import theano.tensor as T
from tree_lstm import ChildSumTreeLSTM, NaryTreeLSTM
import numpy as np
import tree_rnn


class SentimentTreeLSTM(ChildSumTreeLSTM): # can replace with NaryTreeLSTM
    def train_step_inner(self, x, tree, y, y_exists):
        self._check_input(x, tree)
        return self._train(x, tree[:, :-1], y, y_exists)

    def train_step(self, root_node, label):
        x, tree, labels, labels_exist = \
            tree_rnn.gen_nn_inputs(root_node, max_degree=self.degree,
                                   only_leaves_have_vals=False,
                                   with_labels=True)
        y = np.zeros((len(labels), self.output_dim), dtype=theano.config.floatX)
        y[np.arange(len(labels)), labels.astype('int32')] = 1
        loss, pred_y = self.train_step_inner(x, tree, y, labels_exist)
        return loss, pred_y

    def fit(self,X, y, validation_data):
        '''
        Train model with data X and label y
        :param X: Tree list
        :param y: Not need y, as it is writing in the tree itself
        :param validation_data:
        :return:
        '''

        losses = []
        avg_loss = 0.0
        total_data = len(X)
        for i, tree in enumerate(X):
            loss, pred_y = self.train_step(tree, None)  # labels will be determined by model
            losses.append(loss)
            avg_loss = avg_loss * (len(losses) - 1) / len(losses) + loss / len(losses)
            if (i % 500 == 0):
                print('avg loss %.2f at example %d of %d\r' % (avg_loss, i, total_data), end=' ')
        return np.mean(losses)


    def predict_proba(self,X):
        '''

        :param X: tree
        :return:
        '''
        pred_y = self.predict(X)[-1] # root pred is final row
        return pred_y

    def loss_fn_multi(self, y, pred_y, y_exists):
        return T.sum(T.nnet.categorical_crossentropy(pred_y, y) * y_exists)